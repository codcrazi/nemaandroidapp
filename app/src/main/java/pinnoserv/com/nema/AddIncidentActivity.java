package pinnoserv.com.nema;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class AddIncidentActivity extends AppCompatActivity {
    Spinner spLocation,spCategory,spCountry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_incident);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.spLocation = (Spinner)findViewById(R.id.spLocation);
        this.spCategory = (Spinner)findViewById(R.id.spCategory);
        this.spCountry = (Spinner)findViewById(R.id.spCountry);

        populateCountries();
        populateCounties();
        populateCategories();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btnNext = (Button)findViewById(R.id.btnReportIncident);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddIncidentActivity.this, PhotoActivity.class));
            }
        });
    }
    public void populateCountries() {

        String[] array_spinner = getResources().getStringArray(R.array.countries);

        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_spinner);

        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the your spinner
        spCountry.setAdapter(countryAdapter);

    }
    public void populateCounties() {

        String[] array_spinner = getResources().getStringArray(R.array.counties);

        ArrayAdapter<String> countyAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_spinner);

        countyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the your spinner
        spLocation.setAdapter(countyAdapter);

    }
    public void populateCategories() {

        String[] array_spinner = getResources().getStringArray(R.array.categories);

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_spinner);

        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the your spinner
        spCategory.setAdapter(categoryAdapter);

    }
}
