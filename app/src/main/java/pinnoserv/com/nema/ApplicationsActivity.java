package pinnoserv.com.nema;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class ApplicationsActivity extends AppCompatActivity {

    SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerAdapterApplications mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog pdialog;
    private List<Applications> applicationsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applications);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_applications);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               /* refreshContent();*/
            }
        });

        applicationsList = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_app);
        mLayoutManager = new LinearLayoutManager(ApplicationsActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerAdapterApplications(applicationsList, ApplicationsActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        prepareData();
    }

    private void prepareData() {
        Applications in = new Applications("blablabla", "EIA", "EIA Application Form");
        applicationsList.add(in);
        in = new Applications("blablabla", "Waste Water", "Waste Water Application Form");
        applicationsList.add(in);
        in = new Applications("blablabla", "Export Registration", " ");
        applicationsList.add(in);

        mAdapter.notifyDataSetChanged();
    }
}
