package pinnoserv.com.nema;

/**
 * Created by ben on 20/05/2016.
 */
public class Incident {
    String image,title,date;

    public Incident(String image, String title, String date) {
        this.image = image;
        this.title = title;
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
