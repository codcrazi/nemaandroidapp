package pinnoserv.com.nema;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class FAQActivity extends AppCompatActivity {

    SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerAdapterFAQs mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog pdialog;
    private List<Faqs> faqsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_faqs);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               /* refreshContent();*/
            }
        });

        faqsList = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_faqs);
        mLayoutManager = new LinearLayoutManager(FAQActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerAdapterFAQs(faqsList, FAQActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        prepareData();

    }

    private void prepareData() {
        Faqs in = new Faqs("How does one become a registered Environment Impact Assessment (EIA)/ Audit (EA) expert? Click to collapse", "For one to be considered for registration as an EIA expert, one needs to have at least a Bachelors degree in environmental studies or a related field.");
        faqsList.add(in);
        in = new Faqs("Does burning of waste qualify as waste management? Click to collapse", "Burning of waste can only be undertaken in a NEMA licensed incinerator /facility. Hazardous and biomedical waste, for instance must be burned in an incinerator to prevent release of harmful gases to the atmosphere.");
        faqsList.add(in);
        in = new Faqs("How does NEMA respond to oil spills? Click to collapse", "NEMA cordons the site and undertakes inspections to determine the extent of spill and damage.");
        faqsList.add(in);

        mAdapter.notifyDataSetChanged();
    }
}
