package pinnoserv.com.nema;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class IncidentActivity extends AppCompatActivity {

    SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog pdialog;

    private List<Incident> incidentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChoiceAlert();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               /* refreshContent();*/
            }
        });

        incidentList = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(IncidentActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerAdapter(incidentList, IncidentActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        prepareData();
    }

    public void showChoiceAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                IncidentActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("Confirmation").setIcon(R.drawable.logo);
        alertDialog.setMessage("Report Incident Anonymously?");
        // On pressing Settings button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(IncidentActivity.this, AddIncidentActivity.class));
                    }
                });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(IncidentActivity.this, AnonymousActivity.class));
            }
        });
        alertDialog.show();
    }
    private void prepareData() {
        Incident in = new Incident("blablabla", "Sound polution at Kasarani", "18/06/2016");
        incidentList.add(in);
         in = new Incident("blablabla", "Sewage burst in River Rd", "19/06/2016");
        incidentList.add(in);
         in = new Incident("blablabla", "Open waste disposal at Ngara", "10/06/2016");
        incidentList.add(in);

        mAdapter.notifyDataSetChanged();
    }
}
