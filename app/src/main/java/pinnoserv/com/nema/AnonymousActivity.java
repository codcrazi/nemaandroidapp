package pinnoserv.com.nema;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class AnonymousActivity extends AppCompatActivity {
    Spinner spLocation, spCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anonymous);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.spLocation = (Spinner) findViewById(R.id.spAnonLocation);
        this.spCategory = (Spinner) findViewById(R.id.spAnonCategory);
        populateCategories();
        populateCounties();

        Button btnNext = (Button)findViewById(R.id.btnReportAnonIncident);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AnonymousActivity.this, PhotoActivity.class));
            }
        });
    }

    public void populateCounties() {

        String[] array_spinner = getResources().getStringArray(R.array.counties);

        ArrayAdapter<String> countyAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_spinner);

        countyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the your spinner
        spLocation.setAdapter(countyAdapter);

    }

    public void populateCategories() {

        String[] array_spinner = getResources().getStringArray(R.array.categories);

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_spinner);

        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the your spinner
        spCategory.setAdapter(categoryAdapter);

    }
}
