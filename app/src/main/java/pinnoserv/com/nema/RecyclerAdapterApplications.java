package pinnoserv.com.nema;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ben on 22/05/2016.
 */
public class RecyclerAdapterApplications extends RecyclerView.Adapter<RecyclerAdapterApplications.ViewHolder> {

    private static final String TAG = "RecyclerAdapterApplications";

    //private String[] mDataSet;
    private int[] mDataSetTypes;
    private int[] images = {
            R.drawable.eia,R.drawable.water_waste,R.drawable.ereg};

    public static final int FUNDS = 0;
    private int lastPosition = -2;
    private Context context;
    private List<Applications> applicationsList;

    public RecyclerAdapterApplications(List<Applications> applicationsList, Context context) {

        this.applicationsList = applicationsList;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public class ApplicationsViewHolder extends ViewHolder {
        TextView item_title, item_detail;
        ImageView item_image;

        public ApplicationsViewHolder(View v) {
            super(v);
            this.item_title = (TextView) v.findViewById(R.id.tvAppTitle);
            this.item_detail = (TextView) v.findViewById(R.id.tvAppDesc);
            this.item_image = (ImageView) v.findViewById(R.id.ivApp);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_applications, viewGroup, false);

        return new ApplicationsViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        ApplicationsViewHolder holder = (ApplicationsViewHolder) viewHolder;
        Applications apps = applicationsList.get(position);
        holder.item_image.setImageResource(images[position]);


        holder.item_title.setText(apps.getTitle());
        holder.item_detail.setText(apps.getDescription());
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_interploar
                        : R.anim.down_interpolar);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    @Override
    public int getItemCount() {
        return applicationsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return FUNDS;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, Applications data) {
        applicationsList.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Applications data) {
        int position = applicationsList.indexOf(data);
        applicationsList.remove(position);
        notifyItemRemoved(position);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.down_interpolar);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }
}

