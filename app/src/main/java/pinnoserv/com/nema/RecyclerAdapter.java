package pinnoserv.com.nema;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ben on 20/05/2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private static final String TAG = "RecyclerAdapter";

    //private String[] mDataSet;
    private int[] mDataSetTypes;
    private int[] images = {
            R.drawable.sound,R.drawable.sewage,R.drawable.waste};

    public static final int FUNDS = 0;
    private int lastPosition = -2;
    private Context context;
    private List<Incident> incidentList;

    public RecyclerAdapter(List<Incident> incidentList, Context context) {

        this.incidentList = incidentList;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    /*
        public class NewsViewHolder extends ViewHolder {
            TextView headline;
            Button read_more;

            public NewsViewHolder(View v) {
                super(v);
                this.headline = (TextView) v.findViewById(R.id.headline);
                this.read_more = (Button) v.findViewById(R.id.read_more);
            }
        }
*/
    public class IncidentViewHolder extends ViewHolder {
        TextView item_title, item_detail;
        ImageView item_image;

        public IncidentViewHolder(View v) {
            super(v);
            this.item_title = (TextView) v.findViewById(R.id.item_title);
            this.item_detail = (TextView) v.findViewById(R.id.item_date);
            this.item_image = (ImageView) v.findViewById(R.id.item_image);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;
        // if (viewType == FUNDS) {
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_incidents, viewGroup, false);

        return new IncidentViewHolder(v);
        // }
        /*else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_news, viewGroup, false);
            return new NewsViewHolder(v);
            return null;
        }*/
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        // if (viewHolder.getItemViewType() == FUNDS) {

        IncidentViewHolder holder = (IncidentViewHolder) viewHolder;
        Incident incident = incidentList.get(position);
        holder.item_image.setImageResource(images[position]);


        holder.item_title.setText(incident.getTitle());
        holder.item_detail.setText(incident.getDate());
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_interploar
                        : R.anim.down_interpolar);
        holder.itemView.startAnimation(animation);
        lastPosition = position;

        // } else if (viewHolder.getItemViewType() == NEWS) {
           /* NewsViewHolder holder = (NewsViewHolder) viewHolder;
            holder.headline.setText(mDataSet[position]);*/
        // }

    }

    /* private void setAnimation(View viewToAnimate, int position)
     {
         // If the bound view wasn't previously displayed on screen, it's animated
         if (position > lastPosition)
         {
             Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_out);
             viewToAnimate.startAnimation(animation);
             lastPosition = position;
         }
     }*/
    @Override
    public int getItemCount() {
        return incidentList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return FUNDS;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, Incident data) {
        incidentList.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Incident data) {
        int position = incidentList.indexOf(data);
        incidentList.remove(position);
        notifyItemRemoved(position);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.down_interpolar);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }
}

