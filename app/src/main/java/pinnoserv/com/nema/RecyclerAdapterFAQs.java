package pinnoserv.com.nema;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ben on 22/05/2016.
 */
public class RecyclerAdapterFAQs extends RecyclerView.Adapter<RecyclerAdapterFAQs.ViewHolder> {

    private static final String TAG = "RecyclerAdapterFAQs";

    //private String[] mDataSet;
    private int[] mDataSetTypes;


    public static final int FUNDS = 0;
    private int lastPosition = -2;
    private Context context;
    private List<Faqs> faqsList;

    public RecyclerAdapterFAQs(List<Faqs> faqsList, Context context) {

        this.faqsList = faqsList;
        this.context = context;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    /*
        public class NewsViewHolder extends ViewHolder {
            TextView headline;
            Button read_more;

            public NewsViewHolder(View v) {
                super(v);
                this.headline = (TextView) v.findViewById(R.id.headline);
                this.read_more = (Button) v.findViewById(R.id.read_more);
            }
        }
*/
    public class FaqsViewHolder extends ViewHolder {
        TextView item_question, item_answer;


        public FaqsViewHolder(View v) {
            super(v);
            this.item_question = (TextView) v.findViewById(R.id.tvQuestion);
            this.item_answer = (TextView) v.findViewById(R.id.tvAnswer);

        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;
        // if (viewType == FUNDS) {
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_faqs, viewGroup, false);

        return new FaqsViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder,final int position) {
        FaqsViewHolder holder = (FaqsViewHolder) viewHolder;
        Faqs faqs = faqsList.get(position);



        holder.item_question.setText(faqs.getQuestion());
        holder.item_answer.setText(faqs.getAnswer());
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_interploar
                        : R.anim.down_interpolar);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }


    @Override
    public int getItemCount() {
        return faqsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return FUNDS;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, Faqs data) {
        faqsList.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Faqs data) {
        int position = faqsList.indexOf(data);
        faqsList.remove(position);
        notifyItemRemoved(position);
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.down_interpolar);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }
}

