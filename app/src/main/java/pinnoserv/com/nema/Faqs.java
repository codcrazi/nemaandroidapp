package pinnoserv.com.nema;

/**
 * Created by ben on 22/05/2016.
 */
public class Faqs {
    String question,answer;

    public Faqs(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
